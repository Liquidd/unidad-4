<div class="container">
    <div class="row">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>USER NAME</th>
                    <th>Accion</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($logs as $value){?>
                    <tr>
                        <th><?php echo $value['id_usuario'];?></th>
                        <td><?php echo $value['username'];?></td>
                        <td><?php echo $value['accion'];?></td>
                        <td><?php echo $value['fecha'];?></td>
                    </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>