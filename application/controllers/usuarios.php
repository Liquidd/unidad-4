<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('controlador_general.php');
class Usuarios extends Controlador_general {

    public function __construct(){
        parent::__construct();
        $this->load->model("usuarios_m",'',TRUE);
        $this->load->helper('url');
        $this->load->library('session');
        if (!$this->estado_sesion) {
            $this->session->sess_destroy();
            redirect("login");
        }
    }

    public function index()
    {
            $lista_usuarios = $this->usuarios_m->lista_usuarios();
            $array_usuarios = array();
    
            if($lista_usuarios !== FALSE)
            foreach ($lista_usuarios as $key => $values) {
                    $array_usuarios[$key]['id_usuario']=$values['id_usuario'];
                    $array_usuarios[$key]['username']=$values['username'];
                    $array_usuarios[$key]['password']=$values['password'];
                    $array_usuarios[$key]['permisos']=$values['permisos'];
                    $array_usuarios[$key]['estado']=$values['estado'];
    
            }
            $this->view('inicio',array("usuarios" =>$array_usuarios));
    }
    public function test()
    {    
        print_r("  estado sesion :  ".$this->session->userdata('login_state')."   ");
        print_r($this->session->userdata('datos_usuario'));
    }
    public function alta_usuario()
    {
        $data = $this->input->post("data");
        $IDLOG = $this->id_usuario;
        $usuario_logeado = $this->name_user;

        $respuesta = $this->usuarios_m->alta_usuario($data["username"],$data["password"],$data["permisos"],$IDLOG,$usuario_logeado,"INSERT");
        echo $respuesta;
    }
    public function actualizar_usuario()
    {
        $data = $this->input->post("data");

        $IDLOG = $this->id_usuario;
        $usuario_logeado = $this->name_user;

        $id_usuario = $this->input->post("id_usuario");
        $respuesta = $this->usuarios_m->actualizar_usuario($data["username"],$data["password"],$data["permisos"],$IDLOG,$usuario_logeado,"UPDATE",$id_usuario);
        echo $respuesta;
    }
    public function estado_usuario()
    {
        $id_usuario = $this->input->post("id_usuario");
        $estado = $this->input->post("estado");
        $IDLOG = $this->id_usuario;
        $usuario_logeado = $this->name_user;
        $respuesta = $this->usuarios_m->estado_usuario($id_usuario,$estado,$IDLOG,$usuario_logeado);
        echo $respuesta;

    }
    public function detalle()
    {
		$id_usuario = $this->input->post("id_usuario");
        $respuesta = $this->usuarios_m->lista_usuarios($id_usuario);
        echo json_encode($respuesta[0]);
    }
    public function logs()
    {
        if ($this->permisos != "ADMIN") {
            $this->index();
        }
        else {
            $id = $this->id_usuario;
            $lista_logs = $this->usuarios_m->logs_usuario($id);
            $array_logs = array();
    
            if($lista_logs !== FALSE)
            foreach ($lista_logs as $key => $values) {
                    $array_logs[$key]['id_usuario']=$values['id_usuario'];
                    $array_logs[$key]['username']=$values['username'];
                    $array_logs[$key]['accion']=$values['accion'];
                    $array_logs[$key]['fecha']=$values['fecha'];
    
    
            }
            $this->view('logs',array("logs" =>$array_logs));
        }
    }
    public function about()
    {
        $this->view("about");
    }
}