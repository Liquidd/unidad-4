<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Usuarios_m extends CI_Model{

    public function lista_usuarios($id_usuario = null)
    {
        if ($id_usuario == null) {
            $this->db->select('id_usuario,username,password,permisos,estado');
            $this->db->from('usuarios');
            $query=$this->db->get();
            if ($query->num_rows() > 0){
                return $query->result_array();
            }else
            return FALSE;
        }
        else {
            $this->db->select('id_usuario,username,password,permisos,estado');
            $this->db->from('usuarios');
            $this->db->where('id_usuario',$id_usuario);
            $query=$this->db->get();
            if ($query->num_rows() > 0){
                return $query->result_array();
            }else
            return FALSE;
        }
    }
	// CRUD
	public function alta_usuario($username,$password,$permisos,$id_usuario,$usuario_logeado,$accion)
	{
        $this->db->trans_start();
        $success = $this->db->query("call alta_usuario('$username','$password','$permisos','$id_usuario','$usuario_logeado','$accion',@resultado)");
        $success->free_result();
        $query = $this->db->query('select @resultado as out_param');
        $this->db->trans_complete();
        if ($query->row()->out_param > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    public function actualizar_usuario($username,$password,$permisos,$id_usuario,$usuario_logeado,$accion,$IDLOG)
	{        
        $this->db->trans_start();
        $success = $this->db->query("call actualizar_usuario('$username','$password','$permisos','$id_usuario','$usuario_logeado','$accion','$IDLOG',@resultado)");
        $success->free_result();
        $query = $this->db->query('select @resultado as out_param');
        $this->db->trans_complete();
        if ($query->row()->out_param > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

	public function estado_usuario($id_usuario,$estado,$IDLOG,$usuario_logeado)
	{
        if ($estado <= 0) {
            $estado = 1;
            $this->db->trans_start();
            $success = $this->db->query("call estado_usuario('$id_usuario','$estado','$IDLOG','$usuario_logeado',@resultado)");
            $success->free_result();
            $query = $this->db->query('select @resultado as out_param');
            $this->db->trans_complete();
            return "ACTIVED";
        }
        else {
            $estado = 0;
            $this->db->trans_start();
            $success = $this->db->query("call estado_usuario('$id_usuario','$estado','$IDLOG','$usuario_logeado',@resultado)");
            $success->free_result();
            $query = $this->db->query('select @resultado as out_param');
            $this->db->trans_complete();
            return "DELETE";
        }
    }
    public function logs_usuario($id_usuario)
    {
        $this->db->select('id_usuario,username,accion,fecha');
        $this->db->from('logs');
        $this->db->where('id_usuario',$id_usuario);
        $query=$this->db->get();
        if ($query->num_rows() > 0){
            return $query->result_array();
        }else
        return FALSE;
    }

    
}