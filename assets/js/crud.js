var data = {
    "username" : $("#username").val(),
    "password" : $("#password").val(),
    "permisos" : $("#id_permisos").val(),
}
$(document).ready(function(){
  console.log("JSS");
  $(".btn_editar").hide();

  $(".btn_modal").click(function(){
    $( ".btn_guardar").show();
    $( ".btn_editar").hide();
    $("#header_modal").text("Nuevo Usuario");
  });


  $(".btn_estado").on("click", function(){
    var id = $(this).attr("data-id");
    var estado = $(this).attr("data-estado");
    console.log(id);
    console.log("estado: "+estado);


    $.post(base_url+"usuarios/estado_usuario",{
        id_usuario : id,
        estado: estado
    },function(respuesta){
      console.log(respuesta);
      if (respuesta == "ACTIVED") {
        swal({title: "Usuario Activado",icon: "success",}).then((value) => {
          if(value)location.reload();
        });
      }
      else{
        swal({title: "Usuario Desactivado",icon: "error",}).then((value) => {
          if(value)location.reload();
        });
      }
      
    });
  });

  $(".btn_editar").on("click", function(){
      var id = $(this).attr('data-id');
      var data = {
        "username" : $("#username").val(),
        "password" : $("#password").val(),
        "permisos" : $("#id_permisos").val(),
      }
      console.log(data);
      console.log(id);
      $.post(base_url+"usuarios/actualizar_usuario",{
          data : data,
          id_usuario : id
      },function(respuesta){
          console.log(respuesta);
          swal({title: "Usuario Actualizado",icon: "info",}).then((value) => {
              if(value)location.reload();
          });
      });
  })


  $(".btn_guardar").on("click", function(){
    var data = {
      "username" : $("#username").val(),
      "password" : $("#password").val(),
      "permisos" : $("#id_permisos").val(),
    }
    console.log(data);
    $.post(base_url+"usuarios/alta_usuario",{
        data : data
    },function(respuesta){
      console.log(respuesta);
      if(respuesta){
          swal({
          title: "Usuario Registrado",
          icon: "success",
          button: "ACEPTAR",
        }).then((value) => {
          console.log(value);
          location.reload();
        });
      }else{
        swal({title: "Error al Registrar Usuario ",icon: "error",button: "CERRAR"});
      }
    });
  });

  $(".vista_productos").on("click", function(){
    $(".btn_guardar").hide(); 
    $(".btn_editar").show();
    var id = $(this).attr('data-id');
    console.log(id);

    $.post(base_url+"usuarios/detalle",{
            id_usuario : id
    },function(respuesta){
        let datos = JSON.parse(respuesta);
        console.log("----");
        console.log(datos);
        $("#username").val(datos.username);
        $("#password").val(datos.password);
        $("#id_permisos").val(datos.permisos);
        $(".btn_editar").attr('data-id',datos.id_usuario);
        $("#header_modal").text("Actualizar Usuario");
      });
  });
});